﻿using System.Data.Entity;


namespace ProjetoNeppo.Models
{
    public class DataContext : DbContext
    {
        public DataContext() : base("ConnectionDB")
        {
            Database.SetInitializer<DataContext>(
                new CreateDatabaseIfNotExists<DataContext>());

            Database.Initialize(false);
        }

        public DbSet<Pessoa> Pessoas { get; set; }
        public DbSet<Endereco> Enderecos { get; set; }
    }
}