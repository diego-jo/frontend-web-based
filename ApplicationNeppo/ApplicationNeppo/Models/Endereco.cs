﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProjetoNeppo.Models
{
    [Table("Endereco")]
    public class Endereco
    {
        public Endereco()
        {
            Pessoas = new List<Pessoa>();
        }

        public int Id { get; set; }

        [MaxLength(50)]
        [Required(ErrorMessage = "O preenchimento do campo Estado é obrigatório.")]
        public string Estado { get; set; }

        [MaxLength(50)] 
        [Required(ErrorMessage = "O preenchimento do campo Cidade é obrigatório.")]
        public string Cidade { get; set; }

        [MaxLength(50)]
        [Required(ErrorMessage = "O preenchimento do campo Bairro é obrigatório.")]
        public string Bairro { get; set; }

        [MaxLength(100)]
        [Required(ErrorMessage = "O preenchimento do campo Logradouro é obrigatório.")]
        public string Logradouro { get; set; }

        [Required(ErrorMessage = "O preenchimento do campo Numero é obrigatório.")]
        [Range(1, int.MaxValue, ErrorMessage = "O número da residência não pode ser menor que 1.")]
        public int Numero { get; set; }

        public virtual IList<Pessoa> Pessoas { get; set; }
    }
}