﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProjetoNeppo.Models
{
    [Table("Pessoa")]
    public class Pessoa
    {

        public int Id { get; set; }

        [MaxLength(100)]
        [Required(ErrorMessage = "O preenchimento do campo Nome é obrigatório.")]
        public string Nome { get; set; }

        [Required(ErrorMessage = "O preenchimento do campo Sexo é obrigatório.")]
        [MaxLength(1)]
        public string Sexo { get; set; }

        [Required(ErrorMessage = "Data de nascimento é obrigatória")]
        [DisplayFormat(DataFormatString = "dd/MM/yyyy")]
        public DateTime DataNascimento { get; set; }

        [MaxLength(14)]
        [Required(ErrorMessage = "O preenchimento do campo CPF é obrigatório.")]
        public string CPF { get; set; }

        [ForeignKey("Endereco")]
        public int EnderecoId { get; set; }

        public virtual Endereco Endereco { get; set; }

    }
}