﻿using System.Web.Http;
using ProjetoNeppo.Models;
using System.Net;
using System.Linq;
using System;

namespace ApplicationNeppo.Controllers
{
    public class PessoasController : ApiController
    {
        private DataContext context = new DataContext();

        public IHttpActionResult PostPessoa(Pessoa pessoa)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            context.Pessoas.Add(pessoa);

            try
            {
                context.SaveChanges();
            }
            catch (Exception e)
            {
                return BadRequest("Erro: " + e.ToString());
            }

            return CreatedAtRoute("DefaultApi", new { id = pessoa.Id }, pessoa);
        }

        public IHttpActionResult GetPessoa(int id)
        {
            if (id <= 0)
                return BadRequest("O id informado para consulta deve ser maior que zero.");

            var pessoa = context.Pessoas.Find(id);
            if (pessoa == null)
                return NotFound();

            return Ok(pessoa);
        }

        public IHttpActionResult PutPessoa(int id, Pessoa pessoa)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            if (id != pessoa.Id)
                return BadRequest("O Id informado na URL é diferente do Id informado no corpo da requisição.");

            if (context.Pessoas.Count((c => c.Id == id)) == 0)
                return NotFound();

            context.Entry(pessoa).State = System.Data.Entity.EntityState.Modified;

            try
            {
                context.SaveChanges();
            }
            catch (Exception e)
            {
                return BadRequest("Erro: " + e.ToString());
            }
            return StatusCode(HttpStatusCode.NoContent);
        }

        public IHttpActionResult DeletePessoa(int id)
        {
            if (id <= 0)
                return BadRequest("O Id informado na URL deve ser maior que zero.");

            var pessoa = context.Pessoas.Find(id);

            if (pessoa == null)
                return NotFound();

            context.Pessoas.Remove(pessoa);

            try
            {
                context.SaveChanges();
            }
            catch (Exception e)
            {
                return BadRequest("Erro: " + e.ToString());
            }

            return StatusCode(HttpStatusCode.OK);
        }

        public IHttpActionResult GetPessoas(int pagina = 1, int tamanhoPagina = 10)
        {
            if (pagina <= 0 || tamanhoPagina <= 0)
                return BadRequest("Os parametros pagina e tamanhoPagina devem ser maiores que zero.");

            if (tamanhoPagina > 10)
                return BadRequest("O tamanho maxim de página permitido é 10.");

            int totalPaginas = (int)Math.Ceiling(context.Pessoas.Count() / Convert.ToDecimal(tamanhoPagina));

            if (pagina > totalPaginas)
                return BadRequest("A página solicitada não eiste.");

            var pessoas = context.Pessoas.OrderBy(p => p.Nome).Skip(tamanhoPagina * (pagina - 1)).Take(tamanhoPagina);

            return Ok(pessoas.ToList());
        }
    }
}
