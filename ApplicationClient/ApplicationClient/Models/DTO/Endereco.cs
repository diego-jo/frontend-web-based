﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApplicationClient.Models.DTO
{
    public class Endereco
    {
        public List<object> Pessoas { get; set; }
        public int Id { get; set; }
        public string Estado { get; set; }
        public string Cidade { get; set; }
        public string Bairro { get; set; }
        public string Logradouro { get; set; }
        public int Numero { get; set; }
    }
}