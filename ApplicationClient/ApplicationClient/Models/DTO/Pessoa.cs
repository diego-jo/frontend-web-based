﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApplicationClient.Models.DTO
{
    public class Pessoa
    {
        public Endereco Endereco { get; set; }
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Sexo { get; set; }
        public DateTime DataNascimento { get; set; }
        public string CPF { get; set; }
        public int EnderecoId { get; set; }
    }
}