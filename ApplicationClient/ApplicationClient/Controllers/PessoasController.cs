﻿using System;
using System.Net.Http;
using System.Web.Mvc;
using System.Net.Http.Headers;
using ApplicationClient.Models.DTO;
using System.Collections.Generic;

namespace ApplicationClient.Controllers
{
    public class PessoasController : Controller
    {
        HttpClient client = new HttpClient();

        public PessoasController()
        {
            client.BaseAddress = new Uri("http://localhost:53419");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public ActionResult Index()
        {
            List<Pessoa> pessoas = new List<Pessoa>();
            HttpResponseMessage response = client.GetAsync("/api/pessoas").Result;

            try
            {
                if (response.IsSuccessStatusCode)
                {
                    pessoas = response.Content.ReadAsAsync<List<Pessoa>>().Result;
                }
            }
            catch (Exception e)
            {
                throw (e);
            }

            return View(pessoas);
        }

        public ActionResult Details(int id)
        {
            HttpResponseMessage response;
            try
            {
                response = client.GetAsync($"/api/pessoas/{id}").Result;
            }
            catch (Exception e)
            {
                throw (e);
            }

            Pessoa pessoa = response.Content.ReadAsAsync<Pessoa>().Result;
            if (pessoa != null)
                return View(pessoa);

            return HttpNotFound();
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Pessoa pessoa)
        {
            try
            {
                HttpResponseMessage response = client.PostAsJsonAsync<Pessoa>("/api/pessoas", pessoa).Result;
                if (response.StatusCode == System.Net.HttpStatusCode.Created)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    ViewBag.Error = "Erro ao cadastrar pessoa.";
                    return View();
                }
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Edit(int id)
        {
            HttpResponseMessage response;

            try
            {
                response = client.GetAsync($"/api/pessoas/{id}").Result;
            }
            catch (Exception e)
            {
                throw (e);
            }

            Pessoa pessoa = response.Content.ReadAsAsync<Pessoa>().Result;
            if (pessoa != null)
                return View(pessoa);

            return HttpNotFound();
        }

        [HttpPost]
        public ActionResult Edit(int id, Pessoa pessoa)
        {
            try
            {
                HttpResponseMessage response = client.PutAsJsonAsync<Pessoa>($"/api/pessoas/{id}", pessoa).Result;
                if (response.StatusCode == System.Net.HttpStatusCode.NoContent)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    ViewBag.Error = "Erro ao editar pessoa.";
                    return View();
                }
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Delete(int id)
        {
            HttpResponseMessage response;

            try
            {
                response = client.GetAsync($"/api/pessoas/{id}").Result;
            }
            catch (Exception e)
            {
                throw (e);
            }

            Pessoa pessoa = response.Content.ReadAsAsync<Pessoa>().Result;
            if (pessoa != null)
                return View(pessoa);

            return HttpNotFound();
        }

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                HttpResponseMessage response = client.DeleteAsync($"/api/pessoas/{id}").Result;
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    ViewBag.Error = "Erro ao deletar cadastro";
                    return View();
                }
            }
            catch
            {
                return View();
            }
        }
    }
}
