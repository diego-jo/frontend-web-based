﻿using System.Net.Http;
using System.Web.Mvc;

namespace ApplicationClient.Controllers
{
    public class EnderecosController : Controller
    {
        HttpClient client = new HttpClient();

        // GET: Enderecos
        public ActionResult Index()
        {
            return View();
        }

        // GET: Enderecos/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Enderecos/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Enderecos/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Enderecos/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Enderecos/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Enderecos/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Enderecos/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
